package com.moneycode1;

import java.util.ArrayList;
import java.util.List;

import android.telephony.TelephonyManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

import com.fpang.lib.FpangSession;
import com.google.android.gcm.GCMRegistrar;
import com.moneycode1.AdvertisingIdClient.AdInfo;
import com.nextapps.naswall.NASWall;
import com.nextapps.naswall.NASWall.OnAdListListener;
import com.nextapps.naswall.NASWall.OnJoinAdListener;
import com.nextapps.naswall.NASWall.SEX;
import com.nextapps.naswall.NASWallAdInfo;
import com.tnkfactory.ad.TnkSession;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.EventListener;
import com.vungle.publisher.VunglePub;

public class MainActivity extends Activity {

	public static WebView browser;
	String ad_id = null;
	String serverUrl = "http://moneycode.cafe24.com";

	public static String goingForInstallPackage = "";
	public static String goingForInstallKey = "";

	// get the VunglePub instance
	final VunglePub vunglePub = VunglePub.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		FpangSession.init(this);
		vunglePub.init(this, "test");
		vunglePub.setEventListeners(new EventListener() {
			@Override
			public void onAdEnd(boolean b, boolean b1) {

			}

			@Override
			public void onAdStart() {

			}

			@Override
			public void onAdUnavailable(String s) {

			}

			@Override
			public void onAdPlayableChanged(boolean b) {

			}

			@Override
			public void onVideoView(boolean b, int i, int i1) {

			}
		});

		//aid ��������
		new Thread(new Runnable() {

			@Override
			public void run() {


				try {
					AdInfo adInfo = AdvertisingIdClient.getAdvertisingIdInfo(MainActivity.this);
					ad_id = adInfo.getId();
				}
				catch (Exception e){
					// TODO Auto-generated catch block
				}
			}
		}).start();

		Context context = this;
		boolean testMode = false;
		NASWall.init(context, testMode);

		setBrowser();
	}

	@Override
	protected void onStart() {
		super.onStart();

		Context context = this;
		boolean testMode = false;
		NASWall.init(context, testMode);

		// Connect your client

	    /*
	    // Define the outlinks on the page for both app and web
	    Uri appUri1 = Uri.parse("android-app://com.google.developers/appindexingdemo/restaurant1/menu/1");
	    Uri webUrl1 = Uri.parse("http://appindexingdemo.google.com/item/1");

	    Uri appUri2 = Uri.parse("android-app://com.google.developers/appindexingdemo/restaurant1/address/2");
	    Uri webUrl2 = Uri.parse("http://appindexingdemo.google.com/item/2");

	    // Create App Indexing Link objects
	    AppIndexingLink item1 = new AppIndexingLink(appUri1, webUrl1, this.findViewById(R.id.btn1));
	    AppIndexingLink item2 = new AppIndexingLink(appUri2, webUrl2, this.findViewById(R.id.btn2));

	    // Create a list out of these objects
	    List<AppIndexingLink> outlinks = new ArrayList<AppIndexingLink>();
	    outlinks.add(item1);
	    outlinks.add(item2);

	    // Define a title for your current page, shown in autocompletion UI
	    final String TITLE = "App Indexing API Title";

	    // Call the App Indexing API view method
	    AppIndex.AppIndexApi.view(mClient, this, APP_URI, TITLE, WEB_URL, null);
	    */

	}

	@Override
	protected void onStop() {
		super.onStop();
		// Call viewEnd() and disconnect the client

	}

	//String[][] nasData;
	String nasData = "";
	@Override
	protected void onResume() {
		if(goingForInstallPackage != "" && checkInstallPackage(goingForInstallPackage)){
			browser.loadUrl("javascript:finishIgaworksInstall('"+goingForInstallKey+"', '"+goingForInstallPackage+"');");
			goingForInstallKey = "";
			goingForInstallPackage = "";
		}
		super.onResume();
	}


	@Override
	protected void onPause() {
		super.onPause();
		vunglePub.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		vunglePub.onResume();
	}


	public void onWindowFocusChanged(boolean hasFocus){
		super.onWindowFocusChanged(hasFocus);

		if(hasFocus == false) return;
		getViewSize();

	}


	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url){
			view.loadUrl(url);;
			return true;
		}
	}

	@SuppressLint("NewApi")
	private void setBrowser(){
		browser = (WebView)findViewById(R.id.webView1);
		WebSettings mWebSettings = browser.getSettings();
		mWebSettings.setBuiltInZoomControls(true);
		mWebSettings.setMediaPlaybackRequiresUserGesture(false);
		browser.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
		browser.setScrollbarFadingEnabled(false);
		browser.getSettings().setJavaScriptEnabled(true);
		browser.addJavascriptInterface(new AndroidBridge(), "HybridApp");
		browser.setWebViewClient(new webViewClient());
		browser.loadUrl(serverUrl);
	}


	int deviceHieght = 0;
	private void getViewSize(){
		if(deviceHieght == 0){
			deviceHieght = ((RelativeLayout)findViewById(R.id.lo_Root)).getHeight();
			browser.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, deviceHieght));
		}
	}

	private boolean checkInstallPackage(String packageName){
		PackageManager pm = this.getPackageManager();
		List< ApplicationInfo > appList = pm.getInstalledApplications( 0 );
		ApplicationInfo appInfo = null;
		int nSize = appList.size();
		for( int i = 0; i < nSize; i++ ) {
			appInfo = appList.get( i );
			if(appInfo.packageName.equalsIgnoreCase(packageName)) {
				return true;
			}
		}
		return false;
	}


	private ArrayList<NASWallAdInfo> adList = null;
	public void getNasData(String userno){
		Context context = this;
		String userData = userno;
		int age = 0; // ���� (���� ������ ���� ��� 0 ���� ����)
		SEX sex = SEX.SEX_UNKNOWN; // ���� (SEX_UNKNOWN=������������, SEX_MALE=����, SEX_FEMALE=����)
		NASWall.getAdList(context, userData, age, sex, new OnAdListListener() {

			@Override
			public void OnSuccess(ArrayList<NASWallAdInfo> adList) {
				// ������ �������� ����
				//Log.d("nasLoad","1");
				nasData = "";
				MainActivity.this.adList = adList;
				//nasData = new String[adList.size()][5];
				//int dataCount = 0;
				for (NASWallAdInfo adInfo : adList) {
					if(nasData != "") nasData += "#t";
					nasData += adInfo.getAdId() + "#r";
					nasData += adInfo.getTitle() + "#r";
					nasData += adInfo.getMissionText() + "#r";
					nasData += adInfo.getIconUrl() + "#r";
					nasData += adInfo.getRewardPrice() + "#r";
					nasData += adInfo.getIsOnline();
	            	/*
	            	nasData[dataCount][0] = adInfo.getAdId()+"";
	            	nasData[dataCount][1] = adInfo.getTitle();
	            	nasData[dataCount][2] = adInfo.getMissionText();
	            	nasData[dataCount][3] = adInfo.getIconUrl();
	            	nasData[dataCount][4] = adInfo.getRewardPrice()+"";

	            	adInfo.getTitle(); //�����
	                adInfo.getIconUrl(); //������ Url
	                adInfo.getMissionText(); //�������
	                adInfo.getAdPrice(); //�������
	                adInfo.getRewardPrice(); //������
	                adInfo.getRewardUnit(); // �����ݴ���
	                */
					//dataCount++;
				}
			}

			@Override
			public void OnError(int errorCode) {
				// ������ �ҷ����� ����
				Log.d("nasLoad","0");
			}
		});
	}

	public void joinAdNas(int index){
		//Log.d("send", index+"");

		NASWall.joinAd(this, adList.get(index), new OnJoinAdListener() {

			@Override
			public void OnSuccess(NASWallAdInfo adInfo, String url) {
				// ���� ����
				boolean isSuccess = true;
				try {
					Intent intent = Intent.parseUri(url, 0);
					if (intent != null) {
						startActivity(intent);
						isSuccess = true;
					}
				} catch (Exception e) { }

				if (!isSuccess) {
					Toast.makeText(MainActivity.this,
							"캠페인에 참여할 수 없습니다.", Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void OnError(NASWallAdInfo adInfo, int errorCode) {
				// ���� ����
				String message = "[" + errorCode + "] ";
				switch (errorCode) {
					case -10001:
						message += "종료된 캠페인입니다";
						break;
					case -20001:
						message += "이미 참여한 캠페인입니다";
						break;
					default:
						message += "캠페인에 참여할 수 없습니다.";
						break;
				}

				Toast.makeText(MainActivity.this, message,
						Toast.LENGTH_SHORT).show();
			}
		});

	}





	private final Handler handler = new Handler();
	private class AndroidBridge{

		@JavascriptInterface
		public void openAdsyncAd(final String userno){
			handler.post(new Runnable(){
				public void run(){

					FpangSession.setMarket("google");
					FpangSession.setUserId(userno);
					FpangSession.showAdsyncList(MainActivity.this);


				}
			});
		}


		@JavascriptInterface
		public void openIGAAd(final String userno){
			handler.post(new Runnable(){
				public void run(){
					Log.i("", "openIGAAd");
				}
			});
		}

		@JavascriptInterface
		public void openVungleAd(final String userno){
			handler.post(new Runnable(){
				public void run(){
					Log.i("", "openVungleAd");
					if (vunglePub.isAdPlayable()) {
						AdConfig ovverrideConfig = new AdConfig();
						ovverrideConfig.setIncentivized(true);
						vunglePub.playAd(ovverrideConfig);
					} else {
						Toast.makeText(MainActivity.this, R.string.is_not_available_vungle, 1).show();
					}
				}
			});
		}

		@JavascriptInterface
		public void openNasAd(final String userno){
			handler.post(new Runnable(){
				public void run(){
					NASWall.open(MainActivity.this, userno);
				}
			});
		}

		@JavascriptInterface
		public void openTnkAd(String userno){
			TnkSession.setUserName(MainActivity.this, userno);
			TnkSession.showAdList(MainActivity.this, "TNK");
		}


		//�ؽ�Ʈ�۽� ���� �ʱ�ȭ
		@JavascriptInterface
		public void resetNasAd(){
			nasData = "";
		}
		//�ؽ�Ʈ�۽� �������� ��ȯ
		@JavascriptInterface
		public String getNasAd(String userno){
			Log.d("2", userno);
			if(nasData == "") getNasData(userno);
			return nasData;
		}

		//�ؽ�Ʈ�۽� ���� ����
		@JavascriptInterface
		public void joinNasAd(int index){
			joinAdNas(index);
			//Toast.makeText(MainActivity.this, index+"", 1).show();
		}

		//�佺Ʈ ���
		@JavascriptInterface
		public void showToast(final String msg){
			handler.post(new Runnable(){
				public void run(){
					Toast.makeText(MainActivity.this, msg, 1).show();
				}
			});
		}



		//�ٸ������� �ؽ�Ʈ ����
		@JavascriptInterface
		public void sendText(final String title, final String text){
			Intent it = new Intent(android.content.Intent.ACTION_SEND);
			it.setType("text/plain");
			it.putExtra(Intent.EXTRA_TEXT, text);
			startActivity(Intent.createChooser(it, title));
		}

		//�⺻ ��ư�� �߻���Ű��
		@JavascriptInterface
		public void soundEffect_DefaultButton(){
			handler.post(new Runnable(){
				public void run(){
					AudioManager audioManager = (AudioManager) MainActivity.this.getSystemService(Context.AUDIO_SERVICE);
					audioManager.playSoundEffect(SoundEffectConstants.CLICK);
				}
			});

		}


		//gcmId ��Ϲ� ��ȸ
		@JavascriptInterface
		public String regGCM(){
			GCMRegistrar.checkDevice(MainActivity.this);
			GCMRegistrar.checkManifest(MainActivity.this);

			final String regId = GCMRegistrar.getRegistrationId(MainActivity.this);

			if (regId.equals("")) {
				GCMRegistrar.register(MainActivity.this, GCMIntentService.PROJECT_ID);
			} else {
				Log.e("----------", regId);
			}

			return regId;
		}

		//gcm �������
		@JavascriptInterface
		public void unRegGCM(){
			GCMRegistrar.unregister(MainActivity.this);
		}


		//�����̸� ��ȸ
		@JavascriptInterface
		public String getPackageVersionName(String packageName){
			String version = null;
			try {
				PackageInfo i = MainActivity.this.getPackageManager().getPackageInfo(packageName, 0);
				version = i.versionName;
			} catch(NameNotFoundException e) { }

			return version;
		}

		//�����ڵ� ��ȸ
		@JavascriptInterface
		public String getPackageVersionCode(String packageName){
			int version = 0;
			try {
				PackageInfo i = MainActivity.this.getPackageManager().getPackageInfo(packageName, 0);
				version = i.versionCode;
			} catch(NameNotFoundException e) { }

			return String.valueOf(version);
		}

		//���� �÷��� ����� ���� �󼼺��� ������ ��ũ
		@JavascriptInterface
		public void linkGooglePlayStore(String packageName){
			String url = "";

			PackageManager pm = getPackageManager();
			String installed = "0";
			try {
				pm.getPackageInfo("com.android.vending", PackageManager.GET_ACTIVITIES);
				url = "market://details?id="+packageName;
			} catch (PackageManager.NameNotFoundException e) {
				url = "https://market.android.com/details?id="+packageName;
			}

			Intent intent = new Intent(Intent.ACTION_VIEW);

			intent.setData(Uri.parse(url));

			MainActivity.this.startActivity(intent);
		}


		//��ũ�̵�
		@JavascriptInterface
		public void linkURL(String url){
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(url));
			MainActivity.this.startActivity(intent);
		}


		//IMEI�� ��ȯ
		@JavascriptInterface
		public String getIMEI(){
			TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
			return telephonyManager.getDeviceId();
		}


		//IMEI�� ��ȯ
		@JavascriptInterface
		public String getLineNumber(){
			TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
			return telephonyManager.getLine1Number();
		}

		//IMEI�� ��ȯ
		@JavascriptInterface
		public String getAdId(){
			return ad_id;
		}

		@JavascriptInterface
		public void printLog(String tag, String text){
			Log.d(tag, text);
		}

		//������
		@JavascriptInterface
		public void finishApp(){
			MainActivity.this.finish();
		}


		//���ν��� Ȯ��
		@JavascriptInterface
		public String isAppInstalled(String uri) {
			// uri is package path (com.xxx.xx)
			PackageManager pm = getPackageManager();
			String installed = "0";
			try {
				pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
				installed = "1";
			} catch (PackageManager.NameNotFoundException e) {
				installed = "0";
			}
			return installed;
		}


		//��ġ���� ��ũ�̵� �˸�
		@JavascriptInterface
		public void goingForInstall(String key, String packageName){
			goingForInstallKey = key;
			goingForInstallPackage = packageName;
			Log.d("log01",goingForInstallKey + " / " + goingForInstallPackage);
		}
	}

	public void onBackPressed() {
		browser.loadUrl("javascript:backPressed();");
	}
}
