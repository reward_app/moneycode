/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.moneycode1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.google.android.gcm.GCMBaseIntentService;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {
	private static final String tag = "GCMIntentService";
	public static final String PROJECT_ID = "298428853244";
	
	
	 public GCMIntentService() {
	       // super(Constants.PROJECT_ID);
	        // TODO Auto-generated constructor stub
	    }
	    @Override
	    protected void onError(Context arg0, String arg1) {
	        // TODO Auto-generated method stub
	        /**
	         * GCM ���� �߻� �� ó���ؾ� �� �ڵ带 �ۼ��Ѵ�.
	         * ErrorCode�� ���ؼ� GCM Ȩ�������� GCMConstants �� static variable �����Ѵ�. 
	         */
	 
	    }
	    @Override
	    protected void onMessage(Context arg0, Intent arg1) {
	        // TODO Auto-generated method stub
	        /**
	         * GCMServer�� �����ϴ� �޽����� ���� ó�� �� ��� �����ϴ� �޼ҵ��̴�.
	         * Notification, �� ���� ��� �����ڰ� �ϰ� ���� ������ �ش� �޼ҵ忡�� �����Ѵ�.
	         * ���޹��� �޽����� Intent.getExtras().getString(key)�� ���� ������ �� �ִ�.
	         */ 
	    	String msg= arg1.getExtras().getString("msg");
	    	generateNotification(arg0,msg);
	    	 
	    //	arg1.getExtras().getString("me);
	    }
	    @Override
	    protected void onRegistered(Context arg0, String regId) {
	        // TODO Auto-generated method stub
	        /**
	         * GCMRegistrar.getRegistrationId(context)�� ����Ǿ� registrationId�� �߱޹��� ��� �ش� �޼ҵ尡 �ݹ�ȴ�.
	         * �޽��� �߼��� ���� regId�� ������ �����ϵ��� ����.
	         */
	    }
	 
	    @Override
	    protected void onUnregistered(Context arg0, String arg1) {
	        // TODO Auto-generated method stub
	        /**
	         * GCMRegistrar.unregister(context) ȣ��� �ش� ����̽��� registrationId�� ������û�� ��� �ش� �޼ҵ尡 �ݹ�ȴ�.
	         */
	 
	    }
	    public static void generateNotification(Context context, String message) {
	

			int icon = R.drawable.ic_launcher;
			long when = System.currentTimeMillis();

		    String title = context.getString(R.string.app_name);
			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			Intent notificationIntent = new Intent(context, MainActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

		    Notification notification = new NotificationCompat.Builder(context)
				    .setContentTitle(title)
				    .setContentText(message)
				    .setSmallIcon(icon)
				    .setDefaults(Notification.DEFAULT_SOUND)
				    .setAutoCancel(true)
				    .setOnlyAlertOnce(true)
				    .setTicker("알림")
				    .setWhen(when)
				    .setContentIntent(intent)
				    .build();

		    notificationManager.notify(0, notification);
			
			Vibrator vibe = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
		    vibe.vibrate(300);

		}
	}
