function trim(str) {
   str = str.replace(/^\s+/, '');
   for (var i = str.length - 1; i > 0; i--) {
      if (/\S/.test(str.charAt(i))) {
          str = str.substring(0, i + 1);
          break;
       }
    }
    return str;
} 

function setCookie(name, value, expiredays){
	var todayDate = new Date(); 
	todayDate.setDate( todayDate.getDate() +  expiredays ); 
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";";
}
 
function delCookie(name){
	var expireDate = new Date();
	expireDate.setDate( expireDate.getDate() - 1 );
	document.cookie = name + "= " + "; path=/; expires=" + expireDate.toGMTString() + ";";
}

 
function getCookie(name){
	var nameOfCookie = name + "=";
	var x = 0;
	while ( x <= document.cookie.length ) {
	var y = (x+nameOfCookie.length);
	if ( document.cookie.substring( x, y ) == nameOfCookie ) {
		if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 ) endOfCookie = document.cookie.length; 
		return unescape( document.cookie.substring( y, endOfCookie ) );
	}
	x = document.cookie.indexOf( " ", x ) + 1;
	if ( x == 0 )
		break;
	}
	return "";
}

function checkInputN(input){
    var regex = /[^0-9]/g;
    if(regex.test(input.value)){
		window.HybridApp.showToast("숫자만 입력가능합니다.");
        input.value = input.value.replace(regex, '');
        setTimeout(function(){
            input.focus();
        });
    }
}

function checkInputEN(input){
    var regex = /[^0-9a-zA-Z]/g;
    if(regex.test(input.value)){
		window.HybridApp.showToast("영문, 숫자만 입력가능합니다.");
        input.value = input.value.replace(regex, '');
        setTimeout(function(){
            input.focus();
        });
    }
}

function checkInputENH(input){
    var regex = /[^ㄱ-ㅎㅏ-ㅣ가-힣0-9a-zA-Z]/g;
    if(regex.test(input.value)){
		window.HybridApp.showToast("한글, 영문, 숫자만 입력가능합니다.");
        input.value = input.value.replace(regex, '');
        setTimeout(function(){
            input.focus();
        });
    }
}




 
	function addFavorite(title, url){
			// Internet Explorer
		   if(document.all){
			window.external.AddFavorite('http://gogoomam.com','고구맘');
		   }
		   // Google Chrome
		   else if(window.chrome){
			  alert("Ctrl+D키를 누르세요")
		   }
		   // Firefox
		   else if (window.sidebar){
			   window.sidebar.addPanel(title, url, ""); 
			}
			   // Opera
			else if(window.opera && window.print){ // opera 
				var elem = document.createElement('a'); 
				elem.setAttribute('href',url); 
				elem.setAttribute('title',title); 
				elem.setAttribute('rel','sidebar'); 
				elem.click(); 
		   }
	}
	
	function moveScrollTop(){
	if(navigator.appName == 'Netscape')
		document.body.scrollTop = 0;
	else
		document.documentElement.scrollTop = 0;
	}
	
	function createRequest(){
		var request;
		try{
			request = new XMLHttpRequest();
		}catch(ex1){
			var versions = [
				'Msxml2.XMLHTTP.6.0',
				'Msxml2.XMLHTTP.5.0',
				'Msxml2.XMLHTTP.4.0',
				'Msxml2.XMLHTTP.3.0',
				'Msxml2.XMLHTTP',
				'Microsoft.XMLHttp'
			];
			for(var i=0; i<versions.length; i++ ){
				try{
					request = new ActiveXObject(versions);
				}catch(ex2){}
			}
		}
		return request;
	}
 