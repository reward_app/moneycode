<?
	session_start();
	
	
	//임시 로그인계정
	
	$loc = $_REQUEST["loc"];
	$page = $_REQUEST["page"];
	$part = $_REQUEST["part"];
	
	//로그인정보가 없을시
	if(!isset($_SESSION["loginId"]) && strcmp($loc, "login")) echo "<script>location.replace('?loc=login');</script>";
	
	include "func/dbinfo.php";
	
	//색상설정
	$bgColor = "#EEEEEE";
	$uiColor1 = "#da5555";	//메인컬러
	$uiColor2 = "#d24848";	//서브컬러
	$uiColor3 = "#FFFFFF";	//포인트컬러
	
	//메뉴설정
	$arrMenu = array();
	
	$arrMenu[0] = array();
	$arrMenu[0][0] = "user";
	$arrMenu[0][1] = "회원관리";
	$arrMenu[0][2] = array();
	$arrMenu[0][2][0] = array("user_list", "회원리스트");
	
	$arrMenu[1] = array();
	$arrMenu[1][0] = "point";
	$arrMenu[1][1] = "적립/구매내역";
	$arrMenu[1][2] = array();
	$arrMenu[1][2][0] = array("point_history_user", "사용자별 적립내역");
	$arrMenu[1][2][1] = array("purchase_history", "구매 상품 관리");
	
	$arrMenu[2] = array();
	$arrMenu[2][0] = "product";
	$arrMenu[2][1] = "상품관리";
	$arrMenu[2][2] = array();
	$arrMenu[2][2][0] = array("product_category", "카테고리 관리");
	$arrMenu[2][2][1] = array("product_list", "상품 관리");
	$arrMenu[2][2][2] = array("product_stock", "재고 관리");
	
	$arrMenu[3] = array();
	$arrMenu[3][0] = "content";
	$arrMenu[3][1] = "컨텐츠관리";
	$arrMenu[3][2] = array();
	$arrMenu[3][2][0] = array("notice", "공지사항");
	$arrMenu[3][2][1] = array("faq", "FAQ");
	
	$arrMenu[4] = array();
	$arrMenu[4][0] = "setting";
	$arrMenu[4][1] = "앱설정";
	$arrMenu[4][2] = array();
	$arrMenu[4][2][0] = array("setting", "기본설정");
	
?>

<script type="text/javascript">
</script>