<script type="text/javascript">

	function openAlert(alertTitle, alertMsg, buttonText, alertWidth, alertheight, action){
		var alertBG = document.getElementById("alertBG");
		var alertTable = document.getElementById("alertTable");
		
		document.getElementById("alertTitleArea").innerHTML = alertTitle;
		document.getElementById("alertMsgArea").innerHTML = alertMsg;
		document.getElementById("alertButton").value = buttonText;
		
		alertBG.style.display = "block";
		alertTable.style.display = "block";
		
		var parentHeight = alertBG.offsetHeight;
		var parentWidth = alertBG.offsetWidth;
		
		alertTable.style.width = parentWidth * alertWidth;	//팝업 크기(퍼센트)
		alertTable.style.height = parentHeight * alertheight;	//팝업 크기(퍼센트)
		
		var popupHeight = alertTable.offsetHeight;
		var popupWidth = alertTable.offsetWidth;
		
		alertTable.style.left = ((parentWidth-popupWidth)/2) + "px";	//팝업 위치조정
		alertTable.style.top = ((parentHeight-popupHeight)/2) + "px";	//팝업 위치조정
		
		document.getElementById("alertButton").onclick = action;
	}

	function closeAlert(){
		document.getElementById("alertBG").style.display = "none";
		document.getElementById("alertTable").style.display = "none";
	}
	
	function openConfirm(confirmTitle, confirmMsg, button1Text, button2Text, confirmWidth, confirmheight, action1, action2){
		var confirmBG = document.getElementById("confirmBG");
		var confirmTable = document.getElementById("confirmTable");
		
		document.getElementById("confirmTitleArea").innerHTML = confirmTitle;
		document.getElementById("confirmMsgArea").innerHTML = confirmMsg;
		document.getElementById("confirmButton1").value = button1Text;
		document.getElementById("confirmButton2").value = button2Text;
		
		confirmBG.style.display = "block";
		confirmTable.style.display = "block";
		
		var parentHeight = confirmBG.offsetHeight;
		var parentWidth = confirmBG.offsetWidth;
		
		confirmTable.style.width = parentWidth * confirmWidth;	//팝업 크기(퍼센트)
		confirmTable.style.height = parentHeight * confirmheight;	//팝업 크기(퍼센트)
		
		var popupHeight = confirmTable.offsetHeight;
		var popupWidth = confirmTable.offsetWidth;
		
		confirmTable.style.left = ((parentWidth-popupWidth)/2) + "px";	//팝업 위치조정
		confirmTable.style.top = ((parentHeight-popupHeight)/2) + "px";	//팝업 위치조정
		
		if(action1) document.getElementById("confirmButton1").onclick = action1;
		if(action2) document.getElementById("confirmButton2").onclick = action2;
		
		
	}

	function closeConfirm(){
		document.getElementById("confirmBG").style.display = "none";
		document.getElementById("confirmTable").style.display = "none";
	}
	
	//openAlert("타이틀", "메시지", "예",  0.8, 0.4, function(){closeAlert();})
	//openConfirm("타이틀", "메시지", "예", "아뇨", 0.8, 0.5, function(){closeConfirm();}, function(){closeConfirm();});
</script>

<div id="alertBG" class="alertBG"></div>
<table id="alertTable" class="alertTableContainer" cellspacing="0" cellpadding="0">
	<tr style="height:40px;">
		<td id="alertTitleArea" colspan="3" style="width:100%; color:#fab60a; font-weight:bold; font-size:18px;"></td>
	</tr>
	<tr>
		<td>
			<div style="width:100%; height:100%; overflow-y:auto;">
				<table cellspacing="10" cellpadding="0" style="width:100%; height:100%;">
					<tr>
						<td id="alertMsgArea" style="text-align:center; color:#888888;"></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr style="height:49px;">
		<td style="width:100%; padding:3px 6px 6px 6px;">
			<input id="alertButton" type="button" class="bt_infield" style="width:50%; height:100%;" value="확인">
		</td>
	</tr>
</table>


<div id="confirmBG" class="confirmBG"></div>
<table id="confirmTable" class="confirmTableContainer" cellspacing="0" cellpadding="0">
	<tr style="height:40px;">
		<td id="confirmTitleArea" colspan="2" style="width:100%; color:#fab60a; font-weight:bold; font-size:18px;"></td>
	</tr>
	<tr>
		<td colspan="2">
			<div style="width:100%; height:100%; overflow-y:auto;">
				<table cellspacing="10" cellpadding="0" style="width:100%; height:100%;">
					<tr>
						<td id="confirmMsgArea" style="text-align:center; color:#888888;"></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr style="height:49px;">
		<td style="width:50%; padding:3px 3px 6px 6px;">
			<input id="confirmButton1" type="button" class="bt_infield" style="width:100%; height:100%;" value="예">
			
		</td>
		<td style="width:50%; padding:3px 6px 6px 3px;">
			<input id="confirmButton2" type="button" class="bt_infield" style="width:100%; height:100%;" value="아니요">
		</td>
	</tr>
</table>