<?php
/*==========================================================
* 파일명 : itemcallback.php
* 작성자 : 마이티미디어 개발본부/기술지원팀 (dev@mightymedia.co.kr)
* 작성일 : 2013.02
* 수정일 : 2015.04
* 용  도 : 매체사의 앱사용자가 AdSync 서버에서 포인트 적립 완료 시 매체사 서버에서 포인트 전달 받을 경우 샘플

	로그인관련 쿠키나 세션값은 전달이 되지 않음
	(브라우저 호출이 아닌 서버간 호출로 일종의 포인트 전환 처리 데몬역할 페이지임)

	포인트정보를 받아서 매체사 서비스처리 부분 코딩 후 처리결과값을
*==========================================================*/

//매체사의 서비스 처리 관련 include  파일

//AdSync에서 매체사로 전달한 포인트 정보값을 확인할 경우에만 아래 주석을 해제해서 확인
/* error_log 로 저장하여 확인할 수 있다. */
$log_path = "./"; // 로그 위치
$log_file_name = $log_path."test.log"; //로그파일

// 이런 방법으로 해당 파라미터가 제대로 오는지 확인한다..
//	error_log("\r\n".date("Y-m-d H:i:s")." POST : ".json_encode($_POST) ,3,$log_file_name);

$partner	= $_REQUEST['partner'];	// 파트너 앱(서비스) ID
$cust_id	= $_REQUEST['cust_id'];	// 파트너 회원 ID
$ad_no		= $_REQUEST['ad_no'];		// 적립 광고 번호
$seq_id		= $_REQUEST['seq_id'];		// 포인트 적립 고유 ID
$point		= $_REQUEST['point'];		// 적립 포인트
$ad_title	= $_REQUEST['ad_title'];	// 적립 광고 제목
$valid_key	= $_REQUEST['valid_key'];	// 유효성 확인 Key
$date	= $_REQUEST['date'];	// 적립 광고 제목

// 서비스 처리 코딩 후 결과를 기본값
$arr_result = array('Result' => true , 'ResultCode' => 1 ,'ResultMsg' => "성공");

// 전달 받은 파라미터 체크 부분
if(empty($partner) || empty($cust_id) || empty($ad_no) || empty($point) || empty($seq_id) || empty($valid_key) || empty($ad_title)){
	//서비스 처리 코딩 후 결과를 JSON 형식으로 출력
	$arr_result['Result'] = false;
	$arr_result['ResultCode'] = 4;
	$arr_result['ResultMsg'] = "파라미터 오류";
	echo json_encode($arr_result);
	exit;
}

//적립포인트 수신에 대한 유효성 체크 부분
//유효성 확인키 생성: md5(발급받은인증키+서버로전달받은cust_id+서버로전달받은seq_id)
//생성한 유효성 확인키와 서버로전달받은 valid_key 검증
//AdSync로 부터 발급받은 인증키(파트너백오피스 혹은 메일로 발급 받은 인증키)
$issuedAuthKey = "hg7Ir/zOewfY7RH9PCBaTUKzbJQa3pJV3vueEW/W6zI=";

$checkValidKey = md5($issuedAuthKey.$cust_id.$seq_id);
if($checkValidKey != $valid_key)
{
	//서비스 처리 코딩 후 결과를 JSON 형식으로 출력
	$arr_result['Result'] = false;
	$arr_result['ResultCode'] = 2;
	$arr_result['ResultMsg'] = "유효성 확인 key 오류";
	echo json_encode($arr_result);
	exit;
}


// 적립포인트 수신에 대한 중복 체크 처리 부분(단일 고유키키값 seq_id)
//서버로 전달받은 seq_id 값으로 기존 성공적으로 전달받은 seq_id 값이 있는지 DB 검사
//ex)성공적으로 전달받은 적립포인트정보내역 테이블: partner_point
include 'dbinfo.php';
$con = mysql_connect($host,$uname,$pwd) or die("connection failed");
mysql_select_db($db,$con) or die("db selection failed");
mysql_query("set names utf8");

$seqCountResult = mysql_fetch_array(mysql_query("SELECT COUNT(seq_id) FROM ".$tableHeader."_callback_adsync WHERE seq_id = '".$seq_id."';"));

$seq_cnt = $seqCountResult[0];		// 0이상이면 증복이다.
if($seq_cnt != 0){
	$arr_result['Result'] = false;
	$arr_result['ResultCode'] = 3;
	$arr_result['ResultMsg'] = "중복 지급  오류";
	echo json_encode($arr_result);
	exit;
}
mysql_query("INSERT INTO ".$tableHeader."_callback_adsync (partner, cust_id, ad_no, ad_title, point, date, seq_id, valid_key, event_time) VALUES('".$partner."', '".$cust_id."', '".$ad_no."', '".$ad_title."', '".$point."', '".$date."', '".$seq_id."', '".$valid_key."', DATE_FORMAT(now(),GET_FORMAT(DATETIME,'ISO')));");


$userdata = mysql_fetch_array(mysql_query("SELECT recommender, point FROM ".$tableHeader."_userlist WHERE no = '".$cust_id."';"));

mysql_query("INSERT INTO ".$tableHeader."_history_point (userno, state, value, memo, point, balance, event_time) VALUES('".$cust_id."', 1, 1001, '광고적립-".$ad_title."', '".$point."', '".(($userdata[point] *1) + $point)."', DATE_FORMAT(now(),GET_FORMAT(DATETIME,'ISO')));");

mysql_query("UPDATE ".$tableHeader."_userlist SET point = point + ".$point." WHERE no = '".$cust_id."';");


$recommend_bonus = mysql_fetch_array(mysql_query("SELECT recommend_bonus FROM ".$tableHeader."_setting;"));
$recommend_bonus = $recommend_bonus["recommend_bonus"];

if($userdata[recommender] != "0" && $recommend_bonus > 0){
$recommenderBonus = round($reward * ($point * 0.01));

	if($recommenderBonus <= 1) $recommenderBonus = 1;
	$recommenderdata = mysql_fetch_array(mysql_query("SELECT point FROM ".$tableHeader."_userlist WHERE no = '".$userdata[recommender]."';"));


	mysql_query("INSERT INTO ".$tableHeader."_history_point (userno, state, value, memo, point, balance, event_time) VALUES('".$userdata[recommender]."', 1, 1002, '추천인 추가적립(10%)-고객님을 추천한 사용자가 광고를 수행하였습니다.', '".$recommenderBonus."', '".(($recommenderdata[point] *1) + $recommenderBonus)."', DATE_FORMAT(now(),GET_FORMAT(DATETIME,'ISO')));");
	mysql_query("UPDATE ".$tableHeader."_userlist SET point = point + ".$recommenderBonus." WHERE no = '".$userdata[recommender]."';");
}

$arr_result = array('Result' => true , 'ResultCode' => 1 ,'ResultMsg' => "success");
echo json_encode($arr_result);
// 매체사 처리할 리워드 지급 ..


// 매체사 유저에게 리워드 지급 후 성공 Json string return >
//AdSync Noti 서버가 위의 JSON 응답값을 확인 ResultCode값을 Parsing 하여 관련 이력을 처리(AdSync 백오피스에서 확인 가능)
// "1"이 아닌 경우는 일일배치로 본페이지를 재호출함. [1개월간

echo json_encode($arr_result);
exit;

?>