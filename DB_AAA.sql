-- phpMyAdmin SQL Dump
-- version 2.11.5.1
-- http://www.phpmyadmin.net
--
-- 호스트: localhost
-- 처리한 시간: 16-03-09 05:32 
-- 서버 버전: 5.1.45
-- PHP 버전: 5.2.9p2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- 데이터베이스: `moneycode`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_callback_adsync`
--

CREATE TABLE IF NOT EXISTS `moneycode_callback_adsync` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `partner` varchar(30) NOT NULL,
  `cust_id` int(30) NOT NULL,
  `ad_no` varchar(30) NOT NULL,
  `ad_title` varchar(50) NOT NULL,
  `point` varchar(10) NOT NULL,
  `date` varchar(30) NOT NULL,
  `seq_id` varchar(30) NOT NULL,
  `valid_key` varchar(50) NOT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000085 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_callback_igaworks`
--

CREATE TABLE IF NOT EXISTS `moneycode_callback_igaworks` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `usn` varchar(50) NOT NULL,
  `reward_key` varchar(50) NOT NULL,
  `quantity` varchar(50) NOT NULL,
  `campaign_key` varchar(50) NOT NULL,
  `signed_value` varchar(50) NOT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`no`),
  KEY `usn` (`usn`),
  KEY `reward_key` (`reward_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000001 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_callback_nextapps`
--

CREATE TABLE IF NOT EXISTS `moneycode_callback_nextapps` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `user_data` varchar(30) NOT NULL,
  `reward` varchar(50) NOT NULL,
  `ad_id` varchar(30) NOT NULL,
  `ad_key` varchar(50) NOT NULL,
  `ad_name` varchar(50) NOT NULL,
  `ad_type` varchar(10) NOT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100001912 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_callback_tnk`
--

CREATE TABLE IF NOT EXISTS `moneycode_callback_tnk` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `seq_id` varchar(50) NOT NULL,
  `pay_pnt` varchar(50) NOT NULL,
  `md_user_nm` varchar(50) NOT NULL,
  `md_chk` varchar(50) NOT NULL,
  `app_id` varchar(50) NOT NULL,
  `ad_name` varchar(50) NOT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=179 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_faq`
--

CREATE TABLE IF NOT EXISTS `moneycode_faq` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `sort` int(4) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000003 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_history_point`
--

CREATE TABLE IF NOT EXISTS `moneycode_history_point` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `userno` int(9) NOT NULL COMMENT '사용자',
  `state` int(1) NOT NULL COMMENT '상태(1:입금,2:출금)',
  `value` int(4) NOT NULL COMMENT '구분값',
  `memo` varchar(50) NOT NULL COMMENT '메모',
  `point` int(9) NOT NULL COMMENT '포인트',
  `balance` int(9) NOT NULL COMMENT '잔액',
  `event_time` datetime NOT NULL COMMENT '발생시간',
  PRIMARY KEY (`no`),
  KEY `userno` (`userno`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100003167 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_history_purchase`
--

CREATE TABLE IF NOT EXISTS `moneycode_history_purchase` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `product` varchar(20) NOT NULL COMMENT '상품id',
  `userno` int(9) NOT NULL COMMENT '사용자번호',
  `parameter` varchar(100) NOT NULL,
  `state` int(1) NOT NULL COMMENT '상태(1:처리대기중,2:처리완료, 3:거절)',
  `name` varchar(50) NOT NULL COMMENT '상품표시이름',
  `message` varchar(100) NOT NULL COMMENT '전달 메시지',
  `point` int(10) NOT NULL COMMENT '구매한 포인트',
  `event_time` datetime NOT NULL COMMENT '이벤트시간',
  PRIMARY KEY (`no`),
  KEY `userno` (`userno`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000015 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_history_recommend`
--

CREATE TABLE IF NOT EXISTS `moneycode_history_recommend` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `recommender` int(9) NOT NULL COMMENT '추천한자',
  `nominator` int(9) NOT NULL COMMENT '지명받은자',
  `event_time` datetime NOT NULL COMMENT '발생시간',
  PRIMARY KEY (`no`),
  KEY `nominator` (`nominator`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000001 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_notice`
--

CREATE TABLE IF NOT EXISTS `moneycode_notice` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000004 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_pin`
--

CREATE TABLE IF NOT EXISTS `moneycode_pin` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `productno` int(9) NOT NULL,
  `pin` varchar(50) NOT NULL,
  `used` tinyint(1) NOT NULL,
  `used_purchaseno` int(9) NOT NULL,
  `used_time` datetime NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000002 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_product_category`
--

CREATE TABLE IF NOT EXISTS `moneycode_product_category` (
  `no` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sort` int(3) NOT NULL,
  `activate` tinyint(1) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1005 ;

--
-- 테이블의 덤프 데이터 `moneycode_product_category`
--

INSERT INTO `moneycode_product_category` (`no`, `name`, `sort`, `activate`) VALUES
(1001, '현금출금', 1, 1),
(1002, '상품권', 2, 1);

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_product_list`
--

CREATE TABLE IF NOT EXISTS `moneycode_product_list` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(4) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `instruction` varchar(100) NOT NULL,
  `type` int(2) NOT NULL,
  `price` int(9) NOT NULL,
  `activate` tinyint(1) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000022 ;
--
-- 테이블의 덤프 데이터 `moneycode_product_list`
--



-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_product_stock`
--

CREATE TABLE IF NOT EXISTS `moneycode_product_stock` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `productno` int(9) NOT NULL,
  `parameter` varchar(100) NOT NULL,
  `used` tinyint(1) NOT NULL,
  `used_purchaseno` int(9) NOT NULL,
  `used_time` datetime NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000017 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_setting`
--

CREATE TABLE IF NOT EXISTS `moneycode_setting` (
  `no` int(11) NOT NULL,
  `join_bonus` int(9) NOT NULL,
  `input_recommender` int(9) NOT NULL,
  `recommend_bonus` int(9) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 테이블의 덤프 데이터 `moneycode_setting`
--

INSERT INTO `moneycode_setting` (`no`, `join_bonus`, `input_recommender`, `recommend_bonus`) VALUES
(0, 100, 100, 10);


-- --------------------------------------------------------

--
-- 테이블 구조 `moneycode_userlist`
--

CREATE TABLE IF NOT EXISTS `moneycode_userlist` (
  `no` int(9) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL COMMENT '아이디(이메일)',
  `pw` varchar(50) NOT NULL COMMENT '비밀번호',
  `phone` varchar(11) NOT NULL COMMENT '연락처',
  `nick` varchar(20) NOT NULL COMMENT '별명',
  `imei` varchar(20) NOT NULL COMMENT '기기고유번호',
  `gcm_id` text NOT NULL COMMENT 'gcmid',
  `recommend_code` int(9) NOT NULL COMMENT '추천인코드',
  `recommender` int(9) NOT NULL COMMENT '추천한사람',
  `state` int(1) NOT NULL COMMENT '계정상태(1:정상,2:중지,3삭제)',
  `point` int(9) NOT NULL,
  `push` tinyint(1) NOT NULL COMMENT '푸시알람수신여부',
  `joined` datetime NOT NULL COMMENT '가입시간',
  `used` datetime NOT NULL COMMENT '마지막사용시간',
  PRIMARY KEY (`no`),
  KEY `email` (`email`),
  KEY `recommender` (`recommender`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100000783 ;
